# Whatsapp API Tutorial

Hi, this is the implementation example of `https://github.com/pedroslopez/whatsapp-web.js">whatsapp-web.js`

### Reference
- Whatsapp API Tutorial: Part 1 `https://youtu.be/IRRiN2ZQDc8`
- Whatsapp API Tutorial: Part 2 `https://youtu.be/hYpRQ_FE1JI`
- Whatsapp API Tutorial: Tips & Tricks `https://youtu.be/uBu7Zfba1zA`
- Whatsapp API Tutorial: Sending Media File `https://youtu.be/ksVBXF-6Jtc`
- Whatsapp API Tutorial: Deploy to Heroku `https://youtu.be/uSzjbuaHexk`

### How to use?
- Clone or download this repo
- Enter to the project directory
- Run `npm install`
- Run `npm run start`
- Open browser and go to address `http://localhost:8000`
- Scan the QR Code
- Enjoy!

### Notes
As mentioned in the video above, you have to install `nodemon` to run the start script. You can install nodemon globally with `npm i -g nodemon` command.
