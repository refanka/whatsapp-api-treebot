const express = require("express");
const socketIO = require("socket.io");
const qrcode = require("qrcode");
const http = require("http");
const fs = require("fs");
const fileUpload = require("express-fileupload");
const { client } = require("./config/client");
let { sessionCfg, SESSION_FILE_PATH } = require("./config/session");
const { replyMessage } = require("./chatbot/reply");

const port = process.env.PORT || 8000;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload({ debug: true }));

app.get("/", (req, res) => {
  res.sendFile("index.html", {
    root: __dirname,
  });
});

//Chatbot reply
client.on("message", async (msg) => {
  let chat = await msg.getChat();
  if (!chat.isGroup) {
    setTimeout(() => {
      if (msg.body == "/start") {
        msg.reply(replyMessage.start);
      } else if (msg.body == "/FAQ") {
        msg.reply(replyMessage.faq);
      } else if (msg.body == "/kontakCS") {
        msg.reply(replyMessage.kontakCS);
      }
    }, 1000);
  }
});

client.initialize();

// Socket IO
io.on("connection", (socket) => {
  socket.emit("message", "Connecting...");

  client.on("qr", (qr) => {
    console.log("QR RECEIVED", qr);
    qrcode.toDataURL(qr, (err, url) => {
      socket.emit("qr", url);
      socket.emit("message", "QR Code received, scan please!");
    });
  });

  client.on("ready", () => {
    socket.emit("ready", "Whatsapp is ready!");
    socket.emit("message", "Whatsapp is ready!");
  });

  client.on("authenticated", (session) => {
    socket.emit("authenticated", "Whatsapp is authenticated!");
    socket.emit("message", "Whatsapp is authenticated!");
    console.log("AUTHENTICATED", session);
    sessionCfg = session;
    fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), (err) => {
      if (err) {
        console.error(err);
      }
    });
  });

  client.on("auth_failure", (session) => {
    socket.emit("message", "Auth failure, restarting...");
  });

  client.on("disconnected", (reason) => {
    socket.emit("message", "Whatsapp is disconnected!");
    fs.unlinkSync(SESSION_FILE_PATH, (err) => {
      if (err) return console.log(err);
      console.log("Session file deleted!");
    });
    client.destroy();
    client.initialize();
  });
});

server.listen(port, () => {
  console.log("App running on *: " + port);
});
