const replyMessage = {
  start:
    "Selamat datang! Perkenalkan, saya TreeBOT, penjawab otomatis di sini.\nSaya akan berusaha untuk menjawab pertanyaan Anda terkait _Lender_ di Investree. Untuk memulai, silahkan kirimkan perintah berikut:\n\n*/FAQ*\n*/kontakCS*",
  faq:
    "Berikut pertanyaan yang sering diajukan oleh _Lender_ kami:\n*/daftarLender*\n*/verifikasiAkun*\n*/produkPinjaman*\n*/tingkatResiko*\n*/perhitunganBunga*\n*/jaminanPinjaman*\n*/transaksiSBN*\n*/penambahanPendanaan*\n*/produkReksadana*\n*/mobileApps*",
  kontakCS:
    "Hubungi kami melalui nomor _Customer Support_ Investree di *1500886* atau _e-mail_ ke cs@investree.id pada hari *Senin - Jumat pukul 09.00 - 18.00 WIB* atau sesuai hari dan jam kerja operasional.",
};

module.exports = { replyMessage };
