const fs = require("fs");

// Create .json file for auto log in //
const SESSION_FILE_PATH = "../whatsapp-session.json";
let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) {
  sessionCfg = require(SESSION_FILE_PATH);
}

module.exports = { sessionCfg, SESSION_FILE_PATH };
